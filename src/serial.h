#ifndef __TESTER_H__
#define __TESTER_H__

#include "stdint.h"
#include "stdbool.h"

#define BUFFER_SIZE 255


typedef struct {
    uint8_t length;
    uint8_t arr[BUFFER_SIZE];
} ByteBuffer;

int8_t serial_uart_rx_setup(void);
bool parse_msg(const ByteBuffer *buf);

#endif /* __TESTER_H__ */
