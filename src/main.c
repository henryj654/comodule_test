/* main.c - Application main entry point */

/*
 * Copyright (c) 2015-2016 Intel Corporation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/types.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/printk.h>
#include <sys/util.h>
#include <bluetooth/bluetooth.h>

#include <stdio.h>
#include <bluetooth/hci.h>

#include "serial.h"

#include <logging/log.h>
LOG_MODULE_REGISTER(main);

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME) - 1)

K_MUTEX_DEFINE(data_mutex);
struct k_sem update_data_sem;

uint8_t coord1[32];
uint8_t coord2[32];

/*
 * Set Advertisement data. Based on the Eddystone specification:
 * https://github.com/google/eddystone/blob/master/protocol-specification.md
 * https://github.com/google/eddystone/tree/master/eddystone-url
 */
static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR),
	BT_DATA_BYTES(BT_DATA_UUID16_ALL, 0xaa, 0xaa),
	BT_DATA_BYTES(BT_DATA_SVC_DATA16,
		      0xaa, 0xaa, /* random  UUID */
		      0x00, /*  frame type */
		      0x00, /* Calibrated Tx power at 0m */
		      0x00, /* URL Scheme Prefix http://www. */
		      'h', 'e', 'l', 'l', 'o', ' ',
		      'w', 'o', 'r', 'l', 'd', '!',
		      0x00) /* .org */
};

/* Set Scan Response data */
static const struct bt_data sd[] = {
	BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN),
};

static void bt_ready(int err)
{
	char addr_s[BT_ADDR_LE_STR_LEN];
	bt_addr_le_t addr = {0};
	size_t count = 1;

	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	/* Start advertising */
	err = bt_le_adv_start(BT_LE_ADV_NCONN_IDENTITY, ad, ARRAY_SIZE(ad),
			      sd, ARRAY_SIZE(sd));
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}


	/* For connectable advertising you would use
	 * bt_le_oob_get_local().  For non-connectable non-identity
	 * advertising an non-resolvable private address is used;
	 * there is no API to retrieve that.
	 */

	bt_id_get(&addr, &count);
	bt_addr_le_to_str(&addr, addr_s, sizeof(addr_s));

	printk("Beacon started, advertising as %s\n", addr_s);
}

void main(void)
{
	int err;

	k_sem_init(&update_data_sem, 0, 1);

	printk("Starting Beacon Demo\n");

	serial_uart_rx_setup();
	/* Initialize the Bluetooth Subsystem */
	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
	}

	while (true) {
		k_sem_take(&update_data_sem, K_FOREVER);
		printk("Updating payload\r\n");
		char tmp_coords[65];
		k_mutex_lock(&data_mutex, K_FOREVER);
		snprintf(tmp_coords, ARRAY_SIZE(tmp_coords), "%s, %s", coord1, coord2);
		uint8_t len = strlen(tmp_coords);
		
		uint8_t payload[31] = {len, BT_DATA_SVC_DATA16};
		memcpy(payload + 2, tmp_coords, len);

		struct bt_data tmp[3] = {
						BT_DATA_BYTES(BT_DATA_FLAGS, BT_LE_AD_NO_BREDR),
						BT_DATA_BYTES(BT_DATA_UUID16_ALL, 0xaa, 0xaa)
					};
		tmp[2].type = BT_DATA_SVC_DATA16;
		tmp[2].data_len = strlen(payload); // max length <=31 bytes, if coords are too long, they wont be published 
		tmp[2].data = payload;

		k_mutex_unlock(&data_mutex);
		bt_le_adv_update_data(tmp, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	}
}
