#include <drivers/uart.h>
#include <string.h>
#include <stdlib.h>
#include "serial.h"

#include <logging/log.h>

LOG_MODULE_REGISTER(serial);

K_THREAD_STACK_DEFINE(serial_thread_stack, 2048);
static struct k_thread serial_thread;
static k_tid_t serial_thread_handle = NULL;

struct k_sem rx_rdy;
struct k_sem rx_buf_released;
struct k_sem rx_disabled;

extern struct k_sem update_data_sem;
extern struct k_mutex data_mutex; 

const struct device *uart_dev;

ByteBuffer receive_buf;
ByteBuffer receive_buf_cpy;

static const char cmd_id[5] = "gps:";
extern uint8_t coord1[32];
extern uint8_t coord2[32];

void read_callback(const struct device *dev,
			       struct uart_event *evt, void *user_data) {
	ARG_UNUSED(dev);
    // printk("uart evt %d", evt->type);
    // printk("uart data len %d", evt->data.rx.len);

	switch (evt->type) {
        case UART_RX_RDY:
            receive_buf.length = evt->data.rx.len;
            k_sem_give(&rx_rdy);
            break;
        
        // can use buf request and released if stopping and starting rx is not good enough
        case UART_RX_BUF_REQUEST:   
            break;
        case UART_RX_BUF_RELEASED:
            break;
            
        case UART_RX_DISABLED:
            k_sem_give(&rx_disabled);
            break;
        default:
            break;
	}

}

static void serial_thread_entry(void * a, void * b, void * c) {
    (void)a;
    (void)b;
    (void)c;

    printk("Serial thread entry\r\n");

	int ret;
	ret = uart_rx_enable(uart_dev, receive_buf.arr, sizeof(receive_buf.arr), 1 );   // enable and wait w/ 1ms timeout
    printk("rx_enable ret: %d\r\n", ret);
    // uint8_t loop = 0;

    while (1) {
        // disable so we can reset the buffer, without switching buffers
        // our rx protocol is simple enough that we dont have to consider for extra messages
        // after parsing just reenable it
        k_sem_take(&rx_rdy, K_FOREVER);
        memcpy(receive_buf_cpy.arr, receive_buf.arr, sizeof(receive_buf.arr));
        receive_buf_cpy.length = receive_buf.length;
        printk("Receive len: %d\r\n", receive_buf_cpy.length);
        printk("Received in hex :\r\n");
        for(uint16_t i = 0; i < receive_buf.length; ++i) {
            printk("%02x ", receive_buf.arr[i]);
        }
        printk("\r\n");
        
        ret = parse_msg(&receive_buf_cpy);
        printk("ret val %d", ret);

        uart_rx_disable(uart_dev);  
        k_sem_take(&rx_disabled, K_NO_WAIT);
        memset(receive_buf.arr, 0, ARRAY_SIZE(receive_buf.arr));
        ret = uart_rx_enable(uart_dev, receive_buf.arr, sizeof(receive_buf.arr), 1 );   // enable and wait w/ 1ms timeout
    }
}

/**
 * @brief Check if input buf starts with "gps:"
 * 
 * @param buf Bytebuf containin input
 * @return true if starts with "gps:"
 * @return false foesnt start with "gps:"
 */
static bool known_cmd(const ByteBuffer *buf) {
    
    if (strncmp(buf->arr, cmd_id, sizeof(cmd_id) - 1) != 0) {
        return false;
    }
    return true;
}

/**
 * @brief Check if char array contains valid float characters, numbers and 1 dot is allowed
 * 
 * @param arr 
 * @return true is valid
 * @return false is invalid
 */
static bool is_valid(const char *arr) {
    uint8_t dot_count = 0;
    for (size_t i = 0; i < strlen(arr); i++) {
        if (arr[i] == '-') {
            if (i != 0) {
                return false;
            }
            continue;
        }
        if (isdigit(arr[i])) {
            continue;
        }
        if (arr[i] != '.') {
            return false;
        }
        dot_count++;
        if (dot_count > 1) {
            return false;
        }
    }

    return true;
}

bool parse_msg(const ByteBuffer *buf) {
    if ((buf->length) == 0 ) {
        return false;
    }
    if (!known_cmd(buf)) {
        return false;
    }
    printk("Have correct start bytes\r\n");

    char *divider;

    divider = strchr(buf->arr, ',');
    if (!divider) {
        printk("Missing divider\r\n");
        return false;
    }

    uint32_t arr_loc = buf->arr;
    uint32_t div_loc = divider;

    uint16_t indx = div_loc - arr_loc;

    k_mutex_lock(&data_mutex, K_FOREVER);
    memcpy(coord1, buf->arr+4, indx - 4 );
    memcpy(coord2, divider + 1, buf->length - indx);
    k_mutex_unlock(&data_mutex);

    if (!is_valid(coord1)) {
        printk("num 1 is invalid\r\n");
        return false;
    }
    if (!is_valid(coord2)) {
        printk("num 2 is invalid\r\n");
        return false;
    }

    printk("coord 1 is: %s\r\n", coord1);
    printk("coord 2 is: %s\r\n", coord2);
    k_sem_give(&update_data_sem);
    return true;
}

int8_t serial_uart_rx_setup(void) {
    printk("Serial thread initialization\r\n");

    printk("Serial thread semaphore initialization\r\n");
    k_sem_init(&rx_rdy, 0, 1);
    k_sem_init(&rx_buf_released, 0, 1);
    k_sem_init(&rx_disabled, 0, 1);

    uart_dev = device_get_binding("UART_0");

    printk("Setting callback");
	uart_callback_set(uart_dev, read_callback, NULL);



    if(serial_thread_handle) {
        printk("Trying to spawn serial thread twice\r\n");
        return -EEXIST;
        
    }
    printk("Serial thread spawning\r\n");

    serial_thread_handle = k_thread_create(&serial_thread, serial_thread_stack,
                                     K_THREAD_STACK_SIZEOF(serial_thread_stack),
                                     serial_thread_entry,
                                     NULL, NULL, NULL,
                                     1, 0, K_NO_WAIT);
    printk("Serial thread ID: %p\r\n", serial_thread_handle);
    return 0;
}